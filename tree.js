module.exports = {
  Tree: function() {
    this.root = null;
    this.size = 0;

    this.insertPath = function(path) {
      if (typeof path !== 'string') return;
      var pathArr = path.split('/');
      var filteredPathArr = pathArr.filter(function(v) { return v !== ''; });

      var current = this.root;
      for(i in filteredPathArr) {
        if(this.root == null) {
          this.root = new Node(filteredPathArr[i]);
          current = this.root;
          ++this.size;
          continue;
        }

        //Check if branch already exists
        var branchExists = false;
        if(current.value === filteredPathArr[i]) {
          continue;
        }
        for(j in current.branches) {
          if(current.branches[j].value === filteredPathArr[i]) {
            branchExists = true;
            current = current.branches[j];
            break;
          }
        }

        //If we found the branch, continue
        if(branchExists)
          continue;

        //else
        //Make new branch
        var dualLeafNodeSplit = filteredPathArr[i].split('|');
        if(dualLeafNodeSplit.length > 1) { //is dual leaf nodes
          for(i in dualLeafNodeSplit) {
            createNodeAndPushToBranch(current, dualLeafNodeSplit[i]);
          }
        } else {
          var node = createNodeAndPushToBranch(current, filteredPathArr[i]);
          current = node;
        }
        
      }
    };

    function createNodeAndPushToBranch(current, value) {
      var node = new Node(value);
      current.branches.push(node);
      ++this.size;
      return node;
    }

    function Node(value) {
      //Child nodes
      this.branches = [];
      //Value of node
      this.value = value;
    }
  }
};